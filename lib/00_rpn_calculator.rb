# lib/00_rpn_calculator.rb

class RPNCalculator
  def initialize
    @stack = []
  end

  def value
    @stack.each_with_object([]) do |item, values|
      values << if item.is_a?(Symbol)
        values.pop(2).reduce(item)
      else
        item.to_f
      end
    end.last
  end

  def tokens(tokens)
    @stack = tokens.split.map do |token|
      if '+-*/'.include?(token)
        token.to_sym
      else
        token.to_i
      end
    end
  end

  def evaluate(string)
    tokens(string)
    value
  end

  def push(num)
    @stack.push(num)
  end

  def plus
    push_operator(:+)
  end

  def minus
    push_operator(:-)
  end

  def times
    push_operator(:*)
  end

  def divide
    push_operator(:/)
  end

  private

  def push_operator(symbol)
    raise 'calculator is empty' if @stack.empty?
    @stack.push(symbol)
  end
end
